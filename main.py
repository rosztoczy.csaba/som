from itertools import product
from typing import Union

import torch
from torchvision import transforms

from matplotlib import pyplot as plt
from PIL import Image


def get_pic(path: str, basewidth: int = 30) -> torch.Tensor:
    """
    Function to read and scale the input picture. The currently processed picture is saved in ./contents/ as c.png.
    :param str path: path to the input picture.
    :param int basewidth: width size to scale down the picture
    :return: torch.Tensor data: data tensor of the processed picture representing RGB colors
    """
    img = Image.open(path)
    wpercent = (basewidth/float(img.size[0]))
    hsize = int((float(img.size[1])*float(wpercent)))
    img = img.resize((basewidth, hsize), Image.ANTIALIAS)
    img.save('./content/c.png')

    img = Image.open('./content/c.png').convert('RGB')
    to_tensor = transforms.ToTensor()

    # apply transformation and convert to Pytorch tensor
    data = to_tensor(img)
    data = torch.permute(data, (1, 2, 0))
    plt.imshow(data)
    print(data.size())
    return data


class SOM:
    def __init__(self, data: torch.Tensor):
        """
        Class initializing function.
        :param torch.Tensor data: The input tensor of size [x][y][3] representing RGB colors
        """
        self.data = data
        self.nrows, self.ncols, self.ndim = self.data.size()
        self.weights = torch.Tensor()
        self.positions = torch.Tensor()
        self.sample = []
        self.samplepos = ()
        self.dist = torch.Tensor()
        self.bmu = ()
        self.t = 0
        self.__r = 0

    @property
    def r(self) -> int:
        """
        Property method that scales the current radius witch is used to get the neighbors.
        :return: int __r: radius is the size of the picture scaled with a parameter that decreases over time
        """
        self.__r = int((self.nrows + self.ncols) / 2 * (1 - self.t) // 2)
        return self.__r

    @staticmethod
    def show(tensor: torch.Tensor):
        """
        Method for plotting tensors.
        :param torch.Tensor tensor: data to be plotted
        """
        plt.imshow(tensor)
        plt.show()

    def get_weights(self, mode: str = 'random'):
        """
        Method that generates the starting weights.
        :param str mode: 'random' or '4corner' (defaults to 'random')
        """
        if mode == 'random':
            self.weights = torch.rand(self.data.size())
        if mode == '4corner':
            self.weights = torch.zeros(self.data.size())
            for row in range(self.nrows):
                hmul = row / self.nrows * 4.0
                for col in range(self.ncols):
                    wmul = col / self.ncols
                    self.weights[row, col, 0] = (1.0 - wmul) * hmul
                    self.weights[row, col, 1] = wmul * hmul
                    self.weights[row, col, 2] = abs(wmul) * (4.0-hmul)
        self.show(self.weights)

    def get_sample(self, idx: Union[None, int] = None):
        """
        Method that picks the sample, if idx is None than chooses randomly.
        :param Union[None, int] idx: the index of the pixel to choose (position in the flattened tensor)
        """
        if idx is not None:
            row = idx // self.ncols
            col = idx % self.ncols
            self.samplepos = (row, col)
        else:
            self.samplepos = tuple(torch.randint(self.data.size()[0], (2,)))
        self.sample = self.data[self.samplepos]

    def get_dist(self):
        """
        Method that computes distances and chooses the best matching unit.
        :var Tuple self.bmu: tuple of row and col idx of the best matching unit
        """
        self.dist = torch.cdist(self.weights, torch.Tensor(self.sample[None, :]))
        self.bmu = (torch.argmin(self.dist).item() // self.ncols,
                    torch.argmin(self.dist).item() % self.ncols)

    def set_neighbours(self, p: float = float('inf')):
        """
        Method that scales the neighbours so that they become more like the sample.
        :param float p: p parameter of torch.norm
        """
        for rowidx, colidx in product(range(self.nrows), range(self.ncols)):
            d = torch.norm(torch.sub(torch.Tensor((rowidx, colidx)), torch.Tensor(self.bmu)), p=p) / self.r
            t2 = torch.exp((-1 * d * d) / 0.15)
            t2 /= (self.t * 4 + 1)
            if 1 >= d >= 0:
                self.weights[rowidx, colidx] = torch.add(torch.mul(self.weights[rowidx, colidx], 1-t2),
                                                         torch.Tensor(self.sample), alpha=t2.item())

    def run(self, n_times: int, weights: str = 'random', p: float = float('inf'), verbose: int = 0):
        """
        Method for running the algorithm.
        :param int n_times: number of iterations
        :param str weights: type of weight initialization 'random' or '4corner'
        :param float p: p parameter of torch.norm
        :param int verbose: if not 0 prints every step
        """
        self.t = 0
        self.show(self.data)
        self.get_weights(mode=weights)

        t_inc = 1 / n_times
        crounds = n_times // (self.nrows * self.ncols)
        randperm = torch.randperm(self.nrows * self.ncols)
        for _ in range(crounds):
            rpapp = torch.randperm(self.nrows * self.ncols)
            randperm = torch.cat((randperm, rpapp), 0)
        print(f"running {n_times} times, {crounds} complete rounds on {self.nrows * self.ncols} entries")

        for i in randperm:
            self.get_sample(idx=i.item())
            self.get_dist()
            self.set_neighbours(p=p)
            if verbose:
                print([i, self.r])
                print([self.samplepos, self.sample])
                self.show(self.weights)
            self.t += t_inc
            if self.t >= 1:
                self.show(self.weights)
                return

    def bw_check(self, simw: int = 3):
        """
        Method for checking the performance of the current weights.
        :param int simw: the radius of the neighbours witch are used to check the similarity
        """
        max_dist = 0
        t_tensor = torch.zeros(self.nrows, self.ncols)
        for rowidx, colidx in product(range(self.nrows), range(self.ncols)):
            total = 0
            numinave = 0
            for n_ridx, n_cidx in product(range(rowidx-simw, rowidx+simw+1), range(colidx-simw, colidx+simw+1)):
                if self.nrows > n_ridx >= 0 and self.ncols > n_cidx >= 0:
                    total += torch.norm(torch.sub(self.weights[rowidx, colidx], self.weights[n_ridx, n_cidx]), p=2)
                    numinave += 1
            total /= numinave-1
            max_dist = max(total, max_dist)
            t_tensor[rowidx, colidx] = total

        total = torch.sum(t_tensor) / (self.nrows*self.ncols)
        print(f"The current SOMs performance indicator: {total.item()}, simw: {simw}")
        t_tensor = torch.add(torch.mul(torch.div(t_tensor, max_dist), - 255), 255)
        plt.imshow(t_tensor, cmap='gray', vmin=0, vmax=255)
        plt.show()


def main():
    # data = get_pic(path='./content/nzol2.png', basewidth=50)
    data = get_pic(path='./content/dtrees.png', basewidth=50)

    som = SOM(data)

    # som.run(verbose=1, n_times=4, weights='4corner')
    som.run(verbose=0, n_times=500, weights='random', p=2)
    som.bw_check(simw=3)
    # som.show(som.weights)

    # som.run(verbose=0, n_times=300, p=2, weights='4corner')
    # som.run(verbose=0, n_times=900, p=1, weights='random')


if __name__ == '__main__':
    main()
